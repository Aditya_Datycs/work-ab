#!/usr/bin/env python
# coding: utf-8
import unittest
import time
import os
import sys
import json
import pandas as pd
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from ml_rest_services.pagehandler import PageHandler
from datycs.common import config as dpConfig
from datycs.ML.model import Model
from datycs.ML.model import ContentType

import re
import pandas as pd
import pyspark.sql.functions as F

# Install java
#!sudo apt-get install -y openjdk-8-jdk-headless -qq > /dev/null
# os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"
os.environ["JAVA_HOME"] = dpConfig.Config.getInstance().get("java_path/java_home")
os.environ["PATH"] = os.environ["JAVA_HOME"] + "/bin:" + os.environ["PATH"]
# get_ipython().system(' java -version')

# Quick SparkSession start
import sparknlp

spark = sparknlp.start() # for GPU training >> sparknlp.start(gpu=True)

from sparknlp.base import *
from sparknlp.annotator import *


class Vitals(PageHandler):

    ner_prediction_pipeline_BMI = None
    ner_prediction_pipeline_BP = None
    ner_prediction_pipeline_HT = None
    ner_prediction_pipeline_Pulse = None
    ner_prediction_pipeline_Temp = None
    ner_prediction_pipeline_WT = None
    ner_prediction_pipeline_Sp02 = None

    def get(self):
        self.json_response(json.dumps({"module":"Vitals", "version":"0.1.0", "message":"Machine Learning, Vitals NER Module"}))

    def post(self):
        print("Body data: ", self.request.body)
        data = json.loads(self.request.body)
        print("request body data: ", data)
        result = Vitals.process(data['text'])
        self.json_response(json.dumps(result))

    @classmethod
    def load(cls):
        if pd.__version__ < "0.23.2":
            sys.exit('Sorry! Pandas >= 0.23.2 must be installed; however, your version was: ' + pd.__version__)
            
        # Prediction Pipeline
        document = DocumentAssembler().setInputCol("text").setOutputCol("document")

        sentence = SentenceDetector().setInputCols(['document']).setOutputCol('sentence')

        token = Tokenizer().setInputCols(['sentence']).setOutputCol('token')

        # if os.path.isdir("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/cache_pretrained"):
        # if os.path.isdir(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_cache_pretrained")):
        #     print("Already downloaded")
        # else:
        #     print("It is not downloaded.")
        #     glove_embeddings = WordEmbeddingsModel.load('pd_models' + '/glove_100d').setInputCols("document", "token").setOutputCol("embeddings")

        # New Model
        glove_embeddings = WordEmbeddingsModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_cache_pretrained") + '/glove_100d_en_2.4.0_2.4_1579690104032').setInputCols("document", "token").setOutputCol("embeddings")
        
        # Old Model
        # glove_embeddings = XlnetEmbeddings.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_cache_pretrained") + '/xlnet_base_cased_en_2.5.0_2.4_1588074114942').setInputCols("document", "token").setOutputCol("embeddings")

        converter = NerConverter().setInputCols(["document", "token", "ner"]).setOutputCol("ner_span")

        # A1C Model
        # NER_Model_A1C = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_A1C").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        # NER_Model_A1C = NerDLModel.load(dpConfig.Config.getInstance().get("ml/vitals_model_a1c")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        # self.ner_prediction_pipeline_A1C = Pipeline(
        #     stages = [
        #         document,
        #         sentence,
        #         token,
        #         glove_embeddings,
        #         NER_Model_A1C,
        #         converter])

        # BMI Model
        # NER_Model_BMI = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_BMI").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_BMI = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_bmi")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_BMI = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_BMI,
                converter])

        # BP Model
        # NER_Model_BP = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_BP").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_BP = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_bp")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_BP = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_BP,
                converter])

        # HT Model
        # NER_Model_HT = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_HT").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_HT = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_ht")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_HT = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_HT,
                converter])

        # Pulse Model
        # NER_Model_Pulse = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_Pulse").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_Pulse = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_pulse")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_Pulse = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_Pulse,
                converter])

        # Temp Model
        # NER_Model_Temp = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_Temp").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_Temp = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_temp")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_Temp = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_Temp,
                converter])

        # WT Model
        # NER_Model_WT = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_WT").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_WT = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_wt")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_WT = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_WT,
                converter])

        # SpO2 Model
        # NER_Model_SpO2 = NerDLModel.load("/home/purnendu/datycs_tools/datycs/ML/VITALS/pd_models/NerDLModel_SpO2").setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")
        NER_Model_SpO2 = NerDLModel.load(dpConfig.Config.getInstance().get("modules/vitals/models/vitals_model_spo2")).setInputCols(["sentence", "token", "embeddings"]).setOutputCol("ner")

        cls.ner_prediction_pipeline_SpO2 = Pipeline(
            stages = [
                document,
                sentence,
                token,
                glove_embeddings,
                NER_Model_SpO2,
                converter])

    @classmethod
    def process(cls, sec_text):
        # print("sec text: ", sec_text)
        # df_text = self.create_df(sec_text)
        while sec_text.count('\n') < 3:    #this loop was included
            sec_text = sec_text + ' \n '
        out_dict = dict()
        # out_dict["A1C"] = self.get_a1c(self.create_df(sec_text), sec_text)
        out_dict["BMI"] = cls.get_bmi(cls.create_df(sec_text), sec_text)
        out_dict["BP"] = cls.get_bp(cls.create_df(sec_text), sec_text)
        out_dict["HT"] = cls.get_ht(cls.create_df(sec_text), sec_text)
        out_dict["Pulse"] = cls.get_pulse(cls.create_df(sec_text), sec_text)
        out_dict["Temp"] = cls.get_temp(cls.create_df(sec_text), sec_text)
        out_dict["WT"] = cls.get_wt(cls.create_df(sec_text), sec_text)
        out_dict["SpO2"] = cls.get_spo2(cls.create_df(sec_text), sec_text)
        return out_dict

    @classmethod
    def clean_a1c(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '*','~','-','=']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def clean_bmi(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '*', '-', 'dosing', 'measured']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def clean_bp(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '  ', '   ', '*', '-', 'D', 'S', 'sitting']
        for items in remove:
            text = text.replace(items, ' ')
        text = text.replace(' / ', '/')
        text = re.sub(" +", " ", text)
        return text

    @classmethod
    def clean_ht(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '*', '-', 'dosing', 'measured', '/', 'length']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def clean_pulse(cls, text):
        text=text.lower()
        #remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', 'rate', '-']
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '-', '/']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def clean_spo2(cls, text):
        text = str(text)
        text = re.sub(r'[^\w'+"%"+']+', " ", text)
        text = re.sub(" +", " ", text)
        text = text.lower()
        text = text.strip()
        return text

    @classmethod
    def clean_temp(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '*', '-', 'oral']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def clean_wt(cls, text):
        text=text.lower()
        remove = [':', ',', '|', '(', ')', '~', '[', ']', '\n', '\t', '{', '}', '-', 'dosing', 'measured']
        for items in remove:
            text = text.replace(items, ' ')
        return text

    @classmethod
    def find_chunk(cls, df):
        df = df.fillna('O')
        text_pattern = "".join(df["prediction"].tolist())
        if bool(re.search("BI+", text_pattern)):
            pattern_indices = [_.span() for _ in re.finditer("BI+", text_pattern)]
            chunks = list()
            for span in pattern_indices:
                chunks.append(" ".join([str(_) for _ in df.loc[range(span[0], span[1]), "token"].tolist()]))
        elif bool(re.search("B", text_pattern)):
            pattern_indices = [_.span() for _ in re.finditer("B", text_pattern)]
            chunks = list()
            for span in pattern_indices:
                chunks.append(" ".join([str(_) for _ in df.loc[range(span[0], span[1]), "token"].tolist()]))
        elif bool(re.search("I+", text_pattern)): 
            pattern_indices = [_.span() for _ in re.finditer("I+", text_pattern)]
            chunks = list()
            for span in pattern_indices:
                chunks.append(" ".join([str(_) for _ in df.loc[range(span[0], span[1]), "token"].tolist()]))
        else:
            chunks = list()
        
        return chunks

    # def get_a1c(self, df, sec):
    #     df['text'] = df['text'].apply(self.clean_a1c)
    #     df['text'] = df['text'].apply(self.clean_a1c)
    #     empty_data = spark.createDataFrame([['']]).toDF("text")
    #     prediction_model = self.ner_prediction_pipeline_A1C.fit(empty_data)
    #     sample_data = spark.createDataFrame(df)
    #     preds = prediction_model.transform(sample_data)

    #     preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
    #         F.expr("cols['1']").alias("prediction")).toPandas()

    #     out_list = list()
    #     for item in self.find_chunk(preds_df):
    #         out_dict = dict()
    #         # patt_raw = item.split()
    #         # patt = []
    #         # for token_p in patt_raw:
    #         #     if len(token_p)>1:
    #         #         patt.append(token_p)
    #         # g = re.search(r''+patt[0]+'(.*?)'+patt[-1], sec, re.IGNORECASE)
    #         patt = item.split()
    #         reg_pattern = patt[0]
    #         i=1
    #         while i<len(patt):
    #             reg_pattern = reg_pattern + '(.*?)'+patt[i]
    #             i=i+1
    #         g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
    #         if g is not None:
    #             line_no = sec[:g.span()[0]].count("\n")
    #             indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
    #             out_dict["line_no"] = line_no
    #             out_dict["indices_in_line"] = indices_in_line
    #             out_dict["procesed_text"] = item
    #             out_dict["orig_text"] = g.group()
    #         out_list.append(out_dict)
    #     return self.clean_output(out_list)

    @classmethod
    def get_bmi(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_bmi)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_BMI.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()
        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_bp(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_bp)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_BP.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            # patt_raw = item.split()
            # patt = []
            # for token_p in patt_raw:
            #     if len(token_p)>1:
            #         patt.append(token_p)
            # g = re.search(r''+patt[0]+'(.*?)'+patt[-1], sec, re.IGNORECASE)
            patt = item.split()
            # reg_pattern = '(.*?)'+patt[0]
            reg_pattern = patt[0] # new addition instead of prev line
            i=1
            while i<len(patt):
                # reg_pattern = reg_pattern + '(.*?)'+patt[i]
                reg_pattern = reg_pattern + '\W*' + patt[i] # new addition instead of prev line
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                if "\n" in g.group(): # new addition
                    indices_in_line[0] = indices_in_line[0] - 1 # new addition
                    indices_in_line[1] = indices_in_line[1] - 1 # new addition
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_ht(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_ht)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_HT.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_pulse(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_pulse)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_Pulse.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_temp(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_temp)
        df['text'] = df['text'].apply(cls.clean_temp)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_Temp.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_wt(cls, df, sec):
        df['text'] = df['text'].apply(cls.clean_wt)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_WT.fit(empty_data)
        sample_data = spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")).select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def get_spo2(cls, df, sec):
        df['text']=df['text'].apply(cls.clean_spo2)
        empty_data = spark.createDataFrame([['']]).toDF("text")
        prediction_model = cls.ner_prediction_pipeline_SpO2.fit(empty_data)
        sample_data=spark.createDataFrame(df)
        preds = prediction_model.transform(sample_data)

        preds_df = preds.select(F.explode(F.arrays_zip('token.result','ner.result')).alias("cols")) \
        .select(F.expr("cols['0']").alias("token"),
            F.expr("cols['1']").alias("prediction")).toPandas()

        out_list = list()
        for item in cls.find_chunk(preds_df):
            out_dict = dict()
            patt = item.split()
            reg_pattern = patt[0]
            i=1
            while i<len(patt):
                reg_pattern = reg_pattern + '(.*?)'+patt[i]
                i=i+1
            g = re.search(r''+reg_pattern, sec, re.IGNORECASE)
            if g is not None:
                line_no = sec[:g.span()[0]].count("\n")
                indices_in_line = [x - sec[:g.span()[0]].rfind("\n") for x in g.span()]
                out_dict["line_no"] = line_no
                out_dict["indices_in_line"] = indices_in_line
                out_dict["procesed_text"] = item
                out_dict["orig_text"] = g.group()
            out_list.append(out_dict)
        return cls.clean_output(out_list)

    @classmethod
    def create_df(cls, enc):
        text_list = enc.split("\n")
        df_text = pd.DataFrame(columns = ["text"])
        for i in range(len(text_list) - 3):
            df_text = df_text.append({"text" : " ".join([text_list[i], text_list[i + 1], text_list[i + 2]])}, ignore_index = True)
        return df_text

    @classmethod
    def clean_output(cls, out_list):
        if len(out_list) > 0:
            final_output_list = list()
            df_output = pd.DataFrame(out_list).dropna().reset_index(drop = True)
            try:
                for line in set(df_output["line_no"]):
                    dd = df_output.loc[df_output.index[df_output["line_no"] == line]]
                    if dd.shape[0] > 1:
                        max_diff = 0
                        for indx in dd["indices_in_line"]:
                            if (indx[1] - indx[0]) > max_diff:
                                max_diff = indx[1] - indx[0]
                                max_idx = indx
                        final_output_list.append(df_output.loc[(df_output["line_no"] == line) & (df_output["indices_in_line"].apply(str) == str(max_idx))].reset_index(drop = True).T.to_dict()[0])
                    elif dd.shape[0] == 1:
                        final_output_list.append(dd.reset_index(drop = True).T.to_dict()[0])
            except Exception as e:
                print(e)
                
            return final_output_list
        else:
            return []


class TestModel(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestModel, self).__init__(*args, **kwargs)
        start_time = time.time()
        Vitals.load()
        print("Loading time --- %s seconds ---" % (time.time() - start_time))

    def _process(self):
        res= Vitals.process(self.text)
        print("Result: ", res)


    def test_1(self):
        # text = "Vitals Ht: 5 ft 3 in 12/10/2018 Wt: 127 Ibs 12/10/2018 BMI: 225 12/10/2018 03:14 pm 03:16 pm 03:16 pm Resp: 14 Temp: 97.9 ~~F (36.6 ~~C) SpO2: 98% BP 121/79 | Pulse (1) 92"
        start_time = time.time()
        output = self._process(self.text)
        print("Processing time --- %s seconds ---" % (time.time() - start_time))
        print(output)

    def test_2(self):
        # text = "Vitals Ht: 5 ft 3 in 12/10/2018 Wt: 127 Ibs 12/10/2018 BMI: 225 12/10/2018 03:14 pm 03:16 pm 03:16 pm Resp: 14 Temp: 97.9 ~~F (36.6 ~~C) SpO2: 98% BP 121/79 | Pulse (1) 92"
        start_time = time.time()
        for i in range(5):
            output = self._process(self.text)
        print("Processing time --- %s seconds ---" % (time.time() - start_time))
        # print(output)


    text = """History of Present lliness
        History from: patient
        History of Present Illness: Patient presents for f/u of Hypergylcemia, HLD, HTN, B12 def.
        Labs were reviewed and discussed in office today.
        Glucose elevated at 111 per recent labs.
        Pt is not on A statin for his HLD. He has been intolerant to mulitple statins. He has tried Pravastatin, Zocor. Pt reports he
        takes Red Yeast Rice instead. Unsure if he tried Crestor before.
        Pt takes Lisinopril for his HTN.
        Pt gets B12 injections for his B12 def.
        ROS: No complaints of fever, chills, nausea, vomiting, constipation, abd pain.
        ROS otherwise negative
        Care Providers:
        Cardio Dr Pandit
        Ortho Dr Langhans
        Urology Dr. Cole.
        Ortho Dr Kann
        GI Dr Koziara
        Derm Dr Harbovoski, Dr Rau
        Vital Signs
        Previous Height:: 70 inches
        Previous Weight: 197 pounds
        Calculations
        Current Allergies (reviewed today):
        ZOCOR (Critical)
        PRAVACHOL (Critical)
        PRAVASTATIN SODIUM (PRAVASTATIN SODIUM) (Critical)
        Medication List Reviewed During this Office Visit: Done
        Comments: Medications were reviewed today. Patient/family verified understanding and no barriers identified.
        Joseph Kimmell DO
        Pre-Visit Medications (prior to this update):
        HYDROCORTISONE 2.5 % EXTERNAL CREAM (HYDROCORTISONE) Apply twice daily to affected areas as needed for rash; Route: EXTERNAL
        OMEPRAZOLE 20 MG ORAL CAPSULE DELAYED RELEASE (OMEPRAZOLE) 1 capsule by mouth once a day; Route: ORAL
        SUCRALFATE 1GM TABLETS (SUCRALFATE) TAKE ONE TABLET BY MOUTH TWICE DAILY AND UP TO EVERY 6 HRS AS NEEDED DO NOT
        EXCEED 4 DOSES IN 24 HRS
        RED YEAST RICE 600 MG ORAL CAPSULE (RED YEAST RICE EXTRACT) two tabs by mouth twice daily; Route: ORAL
        CYANOCOBALAMIN 1000 MCG/ML INJECTION SOLUTION (CYANOCOBALAMIN) inject 1 ml once a month; Route: INJECTION
        METAMUCIL 48.57 % ORAL POWDER (PSYLLIUM) Take 1 teaspoonful by mouth at bedtime; Route: ORAL
        REFRESH TEARS 0.5 % OPHTHALMIC SOLUTION (CARBOXYMETHYLCELLULOSE SODIUM) 1 drop to each affected eye twice daily; Route:
        OPHTHALMIC
        ASPIRIN 81 MG ORAL TABLET DELAYED RELEASE (ASPIRIN) Take 1
        tab by mouth once daily; Route: ORAL
        BUSPIRONE HCL ORAL TABLET 5 MG (BUSPIRONE HCL) TAKE ONE TABLET BY MOUTH DAILY
        ALLOPURINOL ORAL TABLET 300 MG (ALLOPURINOL) TAKE ONE TABLET BY MOUTH EVERY DAY
        LISINOPRIL 10 MG ORAL TABLET (LISINOPRIL) 1 tablet by mouth once a day
        PROPAFENONE HCL 150 MG ORAL TABLET (PROPAFENONE HCL) one tablet 3 times a day
        Past History
        Past Medical History (reviewed - no changes required): Gout, Anxiety, Hypercholesterolemia, Zetia too costly
        Hypertension, Vitamin B12 dicifiency, Chest pain EKG and stress test normal done as in pt 20/2017
        Colonoscopy 1/29/15 reveals diverticula and polyls Repeat 1/29/19
        Optho Dr Kobaly, Left AC joint sprain, DJD 6/2015
        Dizziness 12/15/15 Epley manuever.
        DRE 7/14, DRE 1/12/2017 slightly enlarged consitent with age. Colonscopy 1/29/15 Dr Koziara repeat in 1/2019
        Surgical History (reviewed - no changes required): card. cath 10/2011 nl., Tonisllectomy, Wisdom teeth extraction
        Right great Toe surgery with perminent hardware
        Basal cell CA nevus removal
        Foot drop surgery
        Social History (reviewed - no changes required): Mariied
        Retired CFO fo rMcDonalds
        2 kids 1 grandkid
        Risk Factors
        Tobacco Use: Never_smoker
        Last Pneumovax: given (01/09/2015 2:36:42 PM)
        Depression Screening
        No
        Are you feeling down, depressed or hopeless? No
        Do you have little interest or pleasure in doing things? No
        Screening for clinical depression was completed using PHQ-2. The result is negative and a follow-up plan is not
        required.
        Previous Falls Assessment: 06/03/2020
        Health Maintenance Review
        Cholesterol: 184 (12/11/2020 7:07:38 AM)
        Next Due Colonoscopy: 01/29/2020
        Health Maintenance Review
        Flu Vaccine: 8/24/2020 - Administered
        Hemoglobin: 14.7 (12/11/2020 7:07:38 AM)
        Hematocrit: 43.6 (12/11/2020 7:07:38 AM)
        MCV: 97 (12/11/2020 7:07:38 AM)
        WBC: 4.9 (12/11/2020 7:07:38 AM)
        Platelets: 228 (12/11/2020 7:07:38 AM)
        TSH: 1.350 (09/12/2014 7:30:00 AM)
        Free T4:1.0 (01/06/2016 8:10:00 AM)
        HgbA1C 5.8 (07/31/2020 7:28:51 AM)
        Fasting Blood sugar: 111 (12/11/2020 7:07:38 AM)
        Total Protein: 6.6 (12/11/2020 7:07:38 AM)
        Albumin: 4.4 (12/11/2020 7:07:38 AM)
        AST: 19 (12/11/2020 7:07:38 AM)
        Alt: 18 (12/11/2020 7:07:38 AM)
        Alkaline Phospotase: 91 (12/11/2020 7:07:38 AM)
        Total Bilirubin:
        1.20 (12/11/2020 7:07:38 AM)
        Direct Bi
        .00 (03/30/2020 7:11:32 AM)
        Hepatitis C antibody Negative (01/06/2016 8:10:00 AM)
        Serum Cholesterol 184 (12/11/2020 7:07:38 AM)
        LDL: 115.4 (12/11/2020 7:07:38 AM)
        HDL: 35 (12/11/2020 7:07:38 AM)
        Triglyceride: 168 (12/11/2020 7:07:38 AM)
        PSA: 5.5 (11/04/2019 7:06:04 AM)
        Sodium: 139 (12/11/2020 7:07:38 AM)
        Potassium: 4.7 (12/11/2020
        7:07:38 AM)
        Chloride: 102 (12/11/2020 7:07:38 AM)
        Bicarbonate 28 (12/11/2020 7:07:38 AM)
        BUN: 15 (12/11/2020 7:07:38 AM)
        Creatinine: 0.8 (12/11/2020 7:07:38 AM)
        GFR:>60 ml/min (12/11/2020 7:07:38 AM)
        Serum Calcium: 9.8 (12/11/2020 7:07:38 AM)
        Serum Magnesium: 2.1 (12/11/2020 7:07:38 AM)
        Uric Acid: 3.7 (12/11/2020 7:07:38 AM)
        Lab Results Reviewed
        Physical Examination
        CONSTITUTIONAL: nad
        EYES: no conjunctival injection appreciated
        ENMT: no external ear lobe abnormalities, midline trachea
        RESPIRATORY: normal resp effort
        MSK: patient seen sitting in chair
        SKIN: clean, dry, intact
        PSYCH: normal mood
        NEURO: alert and oriented, no gross Cranial nerve abnormalities
        Assessment and Plan
        Problem # 1: Hyperglycemia - fasting (ICD-790.29) (ICD10-R73.01) - Stable -
        Patient will continue present
        management.
        Continue to monitor.
        I recommend decreasing simple sugars and high glycemic index foods (white bread, pasta, candy, pastries, potatos, etc.)
        and begin exercising regularly.
        Problem # 2: HYPERLIPIDEMIA (ICD-272.4) (ICD10-E78.5) - Stable -
        Follow newly prescribed regimen.
        CVD Risk score: 22.3%. Indicated for treatment by medication at this time.
        Offered Crestor 5mg PO Twice weekly, a a "last ditch effort" to see if pt can tolerate. Pt agreeable to try.
        Proper usage and potential side effects of medications were discussed and the pt voiced understanding.
        I discussed the risks of uncontrolled cholesterol, including MI, CVA and death.
        | also discussed the options for treatment
        of high cholesterol, including diet/exercise/wt loss as well as the benefits and risks of statin therapy.
        Treatment barriers were identified and discussed
        Problem # 3: HTN (ICD-401.9) (ICD10-110) - Stable -
        Patient will continue present management.
        Continue Lisinopril.
        I encouraged the patient to adopt a healthy lifestyle through diet and regular exercise.
        | asked the patient to keep a log of
        their blood pressure and to bring that to future appointments. Proper usage, possible barriers to use, and potential side
        effects of medications were discussed and the patient voiced understanding.
        | also reviewed treatment goals in regards
        to HTN, namely an avg SBP <130 for CAD equivalent and <140 without.
        | will re-evaluate goals and med response at our
        next visit.
        Problem # 4: B12 deficiency (ICD-266.2) (ICD10-E53.8) - Stable -
        Patient will continue present management.
        Continue B12 injections.
        We discussed the options to replace B12 including oral B12 1000 mcg PO QD or a series of injections
        Instructions for today's visit
        Negative depression screening G8510 11:00-11:15
        XXXXXX verbally consented to treatment and payment of telemedicine services.
        Recommended one month follow-up to assess medication response.
        Additional Plan
        This note was entered by, Mark Brimmeier, Scribe in the presence of and under the direct supervision of Dr. Kimmell.
        The physician has reviewed and verified the accuracy of the information entered.
        All applicable radiographic, laboratory, old medical records and/or consult notes reviewed.
        Today's Orders:
        G8510 - Screening for depression is documented as negative, a follow-up plan is not required [CPT-G8510]
        G0444 - MEDICARE ANNUAL DEPRESSION SCREENING [CPT-G0444]
        Ofc Vst, Est Level IV [CPT-99214]
        Disposition: return to the office in: 1month
        Patient Visit Summary Printed
        Post-Visit Medications (including changes this visit):
        CRESTOR 5 MG ORAL TABLET (ROSUVASTATIN CALCIUM) take one tablet by mouth twice weekly.; Route: ORAL
        HYDROCORTISONE 2.5 % EXTERNAL CREAM (HYDROCORTISONE) Apply twice daily to affected areas as needed
        for rash; Route: EXTERNAL
        OMEPRAZOLE 20 MG ORAL CAPSULE DELAYED RELEASE (OMEPRAZOLE) 1 capsule by mouth once a day; Route:
        ORAL
        SUCRALFATE 1GM TABLETS (SUCRALFATE) TAKE ONE TABLET BY MOUTH TWICE DAILY AND UP TO EVERY 6
        HRS AS NEEDED DO NOT EXCEED 4 DOSES IN 24 HRS
        RED YEAST RICE 600 MG ORAL CAPSULE (RED YEAST RICE EXTRACT) two tabs by mouth twice daily; Route:
        ORAL
        CYANOCOBALAMIN 1000 MCG/ML INJECTION SOLUTION (CYANOCOBALAMIN) inject 1 ml once a month; Route:
        INJECTION
        METAMUCIL 48.57 % ORAL POWDER (PSYLLIUM) Take 1 teaspoonful by mouth at bedtime; Route: ORAL
        REFRESH TEARS 0.5 % OPHTHALMIC SOLUTION (CARBOXYMETHYLCELLULOSE SODIUM) 1 drop to each
        affected eye twice daily; Route: OPHTHALMIC
        ASPIRIN 81 MG ORAL TABLET DELAYED RELEASE (ASPIRIN) Take 1 tab by mouth once daily; Route: ORAL
        BUSPIRONE HCL ORAL TABLET 5 MG (BUSPIRONE HCL) TAKE ONE TABLET BY MOUTH DAILY
        ALLOPURINOL ORAL TABLET 300 MG (ALLOPURINOL) TAKE ONE TABLET BY MOUTH EVERY DAY
        LISINOPRIL 10 MG ORAL TABLET (LISINOPRIL) 1 tablet by mouth once a day
        PROPAFENONE HCL 150 MG ORAL TABLET (PROPAFENONE HCL) one tablet 3 times a day
        Assessed Problems This Visit with Comments:
        Assessed Drug induced myopathy as comment only
        Assessed Hyperglycemia, fasting as comment only
        Assessed HYPERLIPIDEMIA as comment only
        Assessed HTN as comment only
        Assessed B12 deficiency as comment only
        What type of visit is this?
        Real Time Audio and Video
        Medications:
        CRESTOR 5 MG ORAL TABLET (ROSUVASTATIN CALCIUM) take one tablet by mouth twice weekly. #8[Tablet] x 0
        Route:ORAL
        Entered and Authorized by:
        Joseph Kimmell DO
        Electronically signed by: Joseph Kimmell DO on 12/14/2020
        Method used:
        Electronically to
        GIANT EAGLE #0045" (retail)
        9805 McKnight Rd
        Pittsburgh, PA 15237
        Ph: (412) 366-3214
        Fax: (412) 366-4107
        Note to Pharmacy: Route: ORAL;
        RxID:
        1923564211323120
        COVID-19 Telehealth Screening
        High Risk Assessement
        Age >= 65 Yes
        BMI >= 40 No
        Cardiovascular Disease Yes
        Hypertension Yes
        Smoker (previous or current) Yes
        """

        

