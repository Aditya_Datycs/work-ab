import os

vital_list = ['BMI', 'BP', 'HT', 'PULSE', 'SPO2', 'TEMP', 'WT']
for items in vital_list:
    os.system('python3 0-creating_train_test.py '+items+' selected_lines-'+items+'.csv')
    os.system('python3 1-training_ner.py '+items+' glove 10')
