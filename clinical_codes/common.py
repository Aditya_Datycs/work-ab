import difflib
import re

def general_selector(text):
    # Convert Text to Lower
    Lower_List = ['road', 'medical record #', 'mr #', 'street', 'birth', 'yrs', 'alley', 'road', 'rd', 'ave', 
                'avenue', 'store', 'biopsy', 'city', 'state', 'zip', 'parkway', 'way', 'year', 'months', 'mrn', 
                'dob', 'acct', 'addr', 'tel', 'phone', 'fax', ]

    # Don't Convert Text to Lower
    Not_Lower_List = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 
                    'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 
                    'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 
                    'WI', 'WY', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 
                    'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 
                    'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 
                    'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 
                    'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 
                    'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 
                    'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'January', 
                    'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 
                    'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    text = str(text)
    flag = 1
    for item in Lower_List:
        if re.search("\\b"+item+"\\b", text.lower()):
            flag = 0
            return flag
    for item in Not_Lower_List:
        if re.search("\\b"+item+"\\b", text):
            flag = 0
            return flag
    return flag

# Some words might cause confusions. For example "in" and LING has high matching score but we don't want it.
# This varied by codes. 


def not_empty_string(s):
    return s!=""

# Split text into word segment
def split_txt(s):
    try:
        # Cannot ignore hyphen in the middle.
        l = re.split(r"[^\w\-\.]+",s)
        return list(filter(not_empty_string,l))
    except:
        pass

def key_word_matching(a,b,threshold):
    score = matching_score_word(a,b)
    if score>threshold:
        return 1
    else:
        return 0
    
def matching_score_word(a,b):
    a = a.lower()
    b = b.lower()
    
    # Excluding words that might be confused as key words.
    # if a in excludes or b in excludes:
    #     return 0

    seq=difflib.SequenceMatcher(None, a,b)
    return seq.ratio()   

# This function returns the matching score from the key words,which consists of 40% of matching score
def matching_score_keys(text,keys,threshold):
    for key in keys:
        for word in text:
            score = matching_score_word(key,word)
            if score>threshold:
                return 1
    return 0

# This function returns the matching score from the common name,which consists of 60% of matching score.
def matching_score_text(text,common_name,threshold):
    output = []
    for name in common_name:
        score = 0
        for token in text:
#             find max
            if matching_score_word(name,token)>score:
                score = matching_score_word(name,token)
             
            # >0.5 means find it.
            if score>threshold:
                break
# if the score is > 0.5, this means key word is found in the text. 
#         print(score)
        if score>threshold:
            output.append(1)
        else:
            output.append(0)
  
    return sum(output)/len(output)

# This function returns final matching score
def final_matching_score(text,common_name,keyword):
    if len(common_name) > 0:
        text = split_txt(text)
        common_name = split_txt(common_name)
        # print("key_matching ---> ", matching_score_keys(text,keyword,0.55))
        # print("txt_matching ---> ", matching_score_text(text,common_name,0.55))
        return round(0.4 * matching_score_keys(text,keyword,0.65) + 0.6 * matching_score_text(text,common_name,0.55), 2)
    else:
        return 0