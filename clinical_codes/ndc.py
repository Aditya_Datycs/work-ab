import unittest
import time
import re
import os
import sys
import pandas as pd
import requests

# sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))
# from datycs.common import config as dpConfig
# from datycs.clinical_codes_modules.processor import Processor
import common
from common import general_selector, final_matching_score

class Ndc():

    def __init__(self):
        print("subclass constructor called.")
        # super().__init__()

    def load(self):
        self.remove_list = ["\n", "\t", ":", ",", "~", ";", "=", "\(", "\)", "\{", "\}", "\[", "\]", 
                            "\'", "\.", "\|", "\*", "%", "#", r"\d{1,2}/\d{1,2}/\d{1,4}", "/", " +"]
        self.NDC_RegEx = r"((ndc)*\W*(\d{4,5}-\d{3,4}(-\d{1,2})*)+)|((ndc)*\W*(\d{5}-\d{5})+)|((ndc)*\W*(\d{11})+)"
        self.Code_RegEx = r"(\d{4,5}-\d{3,4}-\d{1,2})|(\d{4,5}-\d{3,4})|(\d{5}-\d{5})|(\d{11})"

        self.KeyWords = ["NDC", "National", "Drug"]

    def format_ndc_code(self, NDC_Code):

    	if len(NDC_Code) == 10:
            if NDC_Code.startswith("0"):
                NDC_formatted_code = NDC_Code[:4] + "-" + NDC_Code[4:8] + "-" + NDC_Code[8:]
            else:
                NDC_formatted_code = NDC_Code[:5] + "-" + NDC_Code[5:8] + "-" + NDC_Code[8:]
        elif len(NDC_Code) == 11:
            if NDC_Code.startswith("0"):
                NDC_Code = NDC_Code[1:]
                if NDC_Code.startswith("0"):
                    NDC_formatted_code = NDC_Code[:4] + "-" + NDC_Code[4:8] + "-" + NDC_Code[8:]
                else:
                    NDC_formatted_code = NDC_Code[:5] + "-" + NDC_Code[5:8] + "-" + NDC_Code[8:]
            else:
                if NDC_Code[5] == "0":
                    NDC_formatted_code = NDC_Code[:5] + "-" + NDC_Code[6:9] + "-" + NDC_Code[9:]
                elif NDC_Code[9] == "0":
                    NDC_formatted_code = NDC_Code[:5] + "-" + NDC_Code[5:9] + "-" + NDC_Code[10:]

        return NDC_formatted_code

    def verify_NDC(self, NDC_Code):
        NDC_Code = str(NDC_Code)
        # print("NDC Code before modification ---> ", NDC_Code)
        NDC_Code = format_ndc_code(re.sub("-", "", NDC_Code))
        # print("NDC Code after  modification ---> ", NDC_Code)
        try:
            # url = f"https://www.hipaaspace.com/medical_billing/coding/national.drug.codes/txt/{NDC_Code}"
            url = f"https://api.fda.gov/drug/ndc.json?search=packaging.package_ndc:%22{NDC_Code}%22&limit=1"
            ndc_response = requests.get(url).json()
            ref = ndc_response["results"][0]["generic_name"]
            return [NDC_Code, ref, "Active"]
        except:
            return [NDC_Code, "", "Not Valid"]
    
    def process(self, text):
        try:
            if general_selector(text):
                
                # Cleaning Text
                for item2remove in self.remove_list:
                    text = re.sub(item2remove, " ", text)
                text = text.lower()

                # Getting Matches
                match_list = [sorted(_, key = len, reverse = True) for _ in re.findall(self.NDC_RegEx, text)]
                final_match_list = [item_matched[0].strip() for item_matched in match_list if len(item_matched[0]) > 0]

                # Extracting Codes
                final_codes_list = []
                for item_matched in [re.findall(self.Code_RegEx, match) for match in final_match_list]:
                    if len(item_matched[0]) > 0:
                        final_codes_list.extend(_ for _ in item_matched[0] if len(_) > 0)
                final_codes_list = list(set(final_codes_list))

                # Verifying Codes
                verified_codes_list = list()
                for code in final_codes_list:
                    Clinical_Code, Clinical_Code_Name, Clinical_Code_Status = self.verify_NDC(code)
                    Clinical_Code_Score = final_matching_score(text, Clinical_Code_Name, self.KeyWords)
                    verified_codes_list.append({"code" : Clinical_Code, "name" : Clinical_Code_Name, "status" : Clinical_Code_Status, "score" : Clinical_Code_Score, "threshold" : 0.3})
                
                return verified_codes_list
            else:
                return []
        except:
            return []



class standalone_test(unittest.TestCase):
    def test(self):
        ndc = Ndc()
        clinical_codes_modules = Processor.get_clinical_codes()
        for clinical_code_module in clinical_codes_modules:
            print("module: ", clinical_code_module)
            clinical_code_module.load()
            res = clinical_code_module.process("0.5 mL single dose vial (preservative & latex free) 2350 ~~90686 99215 12345 CO0GB6-SL | emt oro rn")
            print("result: ", res)
            # self.assertEqual(res["Active"], ["90686"])
