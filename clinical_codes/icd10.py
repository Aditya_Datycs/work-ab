import unittest
import time
import re
import os
import sys
import pandas as pd
from bs4 import BeautifulSoup
from urllib.request import urlopen

# sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))
# from datycs.common import config as dpConfig
# from datycs.clinical_codes_modules.processor import Processor
import common
from common import general_selector, final_matching_score


class Icd10():

    def __init__(self):
        print("subclass constructor called.")
        # super().__init__()

    def load(self):
        self.ICD10_RegEx = r"[A-Z1]{1}\d{2}\.?\d{0,2}"

        self.KeyWords = ["ICD", "1CD"] # "HCC"

    def verify_ICD10(self, ICD10_Code):
        try:
            url = f"https://www.icd10data.com/search?s={ICD10_Code}"
            html = urlopen(url).read()
            soup = BeautifulSoup(html, features="html.parser")

            # kill all script and style elements
            for script in soup(["script", "style"]):
                script.extract()    # rip it out

            # get text
            text = soup.get_text()

            # break into lines and remove leading and trailing space on each
            lines = (line.strip() for line in text.splitlines())
            # break multi-headlines into a line each
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            # drop blank lines
            text = '\n'.join(chunk for chunk in chunks if chunk)

            ref = re.findall(r'(\[convert to ICD-9-CM\]|(ICD-10-CM Diagnosis Code ' + ICD10_Code + ' ))(.*?)ICD-10-CM Diagnosis Code', text)
            
            return [ICD10_Code, ref[0][-1], "Active"]
        except:
            return [ICD10_Code, "", "Not Valid"]

    def process(self, text):
        try:
            if general_selector(text):
                
                text = text.replace(',','.')
                code = re.findall(self.ICD10_RegEx, text)
                final_codes_list = []
                for items in code:
                    s = items
                    if s[0]=='1':
                        s = 'I'+s[1:]
                    final_codes_list.append(s.strip())
                final_codes_list =  list(set(final_codes_list))

                # Verifying Codes
                verified_codes_list = list()
                for code in final_codes_list:
                    # print("Verify ---> ", code)
                    Clinical_Code, Clinical_Code_Name, Clinical_Code_Status = self.verify_ICD10(code)
                    if Clinical_Code_Name == "":
                    	# print("First Try ---> ", Clinical_Code_Name)
                    	time.sleep(0.5)
                    	Clinical_Code, Clinical_Code_Name, Clinical_Code_Status = self.verify_ICD10(code)
                    	# print("Second Try ---> ", Clinical_Code_Name)
                    Clinical_Code_Score = final_matching_score(text, Clinical_Code_Name, self.KeyWords)
                    verified_codes_list.append({"code" : Clinical_Code, "name" : Clinical_Code_Name, "status" : Clinical_Code_Status, "score" : Clinical_Code_Score, "threshold" : 0.3})

                return verified_codes_list
            else:
            	# print("not selected")
            	return []
        except:
        	# print("error")
        	return []



class standalone_test(unittest.TestCase):
    def test(self):
        icd10 = Icd10()
        clinical_codes_modules = Processor.get_clinical_codes()
        for clinical_code_module in clinical_codes_modules:
            print("module: ", clinical_code_module)
            clinical_code_module.load()
            res = clinical_code_module.process("0.5 mL 250.0 E44 N18.3 J44.3 single dose vial (preservative & latex free) 2350 ~~90686 99215 12345 CO0GB6-SL | E11.9 emt oro rn")
            print("result: ", res)
            # self.assertEqual(res["Active"], ["90686"])

