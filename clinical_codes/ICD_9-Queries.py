
# coding: utf-8

# In[15]:


code = '575.9'


# In[16]:


parent_str = code.split('.')[0]
parent = int(parent_str)


# In[17]:


if parent<140:
    if parent>0 and parent<10:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/001-009/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>9 and parent<19:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/010-018/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>19 and parent<28:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/020-027/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>29 and parent<42:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/030-041/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==42:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/042-042/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>44 and parent<50:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/045-049/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>49 and parent<60:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/050-059/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>59 and parent<67:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/060-066/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>69 and parent<80:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/070-079/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>79 and parent<89:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/080-088/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>89 and parent<100:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/090-099/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>99 and parent<105:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/100-104/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>109 and parent<119:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/110-118/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>119 and parent<130:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/120-129/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>129 and parent<137:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/130-136/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>136 and parent<140:
        query = 'http://www.icd9data.com/2015/Volume1/001-139/137-139/'+str(parent_str)+'/'+str(code)+'.htm'

elif parent>139 and parent<240:
    if parent>139 and parent<150:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/140-149/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>149 and parent<160:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/150-159/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>159 and parent<166:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/160-165/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>169 and parent<177:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/170-176/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>178 and parent<190:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/179-189/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>189 and parent<200:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/190-199/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>199 and parent<210:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/200-209/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>209 and parent<230:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/210-229/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>229 and parent<235:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/230-234/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>234 and parent<239:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/235-238/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==239:
        query = 'http://www.icd9data.com/2015/Volume1/140-239/239-239/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>239 and parent<280:
    if parent>239 and parent<247:
        query = 'http://www.icd9data.com/2015/Volume1/240-279/240-246/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>248 and parent<260:
        query = 'http://www.icd9data.com/2015/Volume1/240-279/249-259/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>259 and parent<270:
        query = 'http://www.icd9data.com/2015/Volume1/240-279/260-269/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>269 and parent<280:
        query = 'http://www.icd9data.com/2015/Volume1/240-279/270-279/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>279 and parent<290:
    if parent==280:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/280/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==281:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/281/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==282:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/282/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==283:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/283/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==284:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/284/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==285:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/285/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==286:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/286/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==287:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/287/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==288:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/288/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==289:
        query = 'http://www.icd9data.com/2015/Volume1/280-289/289/'+str(parent_str)+'/'+str(code)+'.htm'
    
elif parent>289 and parent<320:
    if parent>289 and parent<295:
        query = 'http://www.icd9data.com/2015/Volume1/290-319/290-294/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>294 and parent<300:
        query = 'http://www.icd9data.com/2015/Volume1/290-319/295-299/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>299 and parent<317:
        query = 'http://www.icd9data.com/2015/Volume1/290-319/300-316/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>316 and parent<320:
        query = 'http://www.icd9data.com/2015/Volume1/290-319/317-319/'+str(parent_str)+'/'+str(code)+'.htm'

elif parent>319 and parent<390:
    if parent>319 and parent<328:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/320-327/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>329 and parent<338:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/330-337/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==338:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/338-338/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==339:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/339-339/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>339 and parent<350:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/340-349/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>349 and parent<360:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/350-359/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>359 and parent<380:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/360-379/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>379 and parent<390:
        query = 'http://www.icd9data.com/2015/Volume1/320-389/380-389/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>389 and parent<460:
    if parent>389 and parent<393:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/390-392/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>392 and parent<399:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/393-398/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>400 and parent<406:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/401-405/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>409 and parent<415:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/410-414/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>414 and parent<418:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/415-417/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>419 and parent<430:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/420-429/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>429 and parent<439:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/430-438/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>439 and parent<450:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/440-449/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>449 and parent<460:
        query = 'http://www.icd9data.com/2015/Volume1/390-459/450-459/'+str(parent_str)+'/'+str(code)+'.htm'
    
elif parent>459 and parent<520:
    if parent>459 and parent<467:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/460-466/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>469 and parent<479:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/470-478/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>479 and parent<489:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/480-488/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>489 and parent<497:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/490-496/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>499 and parent<510:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/500-508/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>509 and parent<520:
        query = 'http://www.icd9data.com/2015/Volume1/460-519/510-519/'+str(parent_str)+'/'+str(code)+'.htm'
    
elif parent>519 and parent<580:
    if parent>519 and parent<530:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/520-529/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>529 and parent<540:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/530-539/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>539 and parent<544:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/540-543/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>549 and parent<554:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/550-553/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>554 and parent<559:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/555-558/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>559 and parent<570:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/560-569/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>569 and parent<580:
        query = 'http://www.icd9data.com/2015/Volume1/520-579/570-579/'+str(parent_str)+'/'+str(code)+'.htm'
    
elif parent>579 and parent<630:
    if parent>579 and parent<590:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/580-589/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>589 and parent<600:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/590-599/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>599 and parent<609:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/600-608/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>609 and parent<613:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/610-612/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>613 and parent<617:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/614-616/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>616 and parent<630:
        query = 'http://www.icd9data.com/2015/Volume1/580-629/617-629/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>629 and parent<680:
    if parent>629 and parent<640:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/630-639/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>639 and parent<650:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/640-649/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>649 and parent<660:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/650-659/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>659 and parent<670:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/660-669/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>669 and parent<678:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/670-677/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>677 and parent<680:
        query = 'http://www.icd9data.com/2015/Volume1/630-679/678-679/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>679 and parent<710:
    if parent>679 and parent<687:
        query = 'http://www.icd9data.com/2015/Volume1/680-709/680-686/'+str(parent_str)+'/'+str(code)+'.htm'
    if parent>689 and parent<699:
        query = 'http://www.icd9data.com/2015/Volume1/680-709/690-698/'+str(parent_str)+'/'+str(code)+'.htm'
    if parent>699 and parent<710:
        query = 'http://www.icd9data.com/2015/Volume1/680-709/700-709/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>709 and parent<740:
    if parent>709 and parent<720:
        query = 'http://www.icd9data.com/2015/Volume1/710-739/710-719/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>719 and parent<725:
        query = 'http://www.icd9data.com/2015/Volume1/710-739/720-724/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>724 and parent<730:
        query = 'http://www.icd9data.com/2015/Volume1/710-739/725-729/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>729 and parent<740:
        query = 'http://www.icd9data.com/2015/Volume1/710-739/730-739/'+str(parent_str)+'/'+str(code)+'.htm'
    
elif parent>739 and parent<760:
    if parent==740:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/740/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==741:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/741/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==742:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/742/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==743:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/743/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==744:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/744/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==745:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/745/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==746:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/746/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==747:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/747/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==748:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/748/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==749:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/749/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==750:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/750/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==751:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/751/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==752:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/752/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==753:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/753/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==754:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/754/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==755:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/755/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==756:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/756/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==757:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/757/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==758:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/758/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent==759:
        query = 'http://www.icd9data.com/2015/Volume1/740-759/759/'+str(parent_str)+'/'+str(code)+'.htm'

elif parent>759 and parent<780:
    if parent>759 and parent<764:
        query = 'http://www.icd9data.com/2015/Volume1/760-779/760-763/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>763 and parent<780:
        query = 'http://www.icd9data.com/2015/Volume1/760-779/764-779/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>779 and parent<800:
    if parent>779 and parent<790:
        query = 'http://www.icd9data.com/2015/Volume1/780-799/780-789/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>789 and parent<797:
        query = 'http://www.icd9data.com/2015/Volume1/780-799/790-796/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>796 and parent<800:
        query = 'http://www.icd9data.com/2015/Volume1/780-799/797-799/'+str(parent_str)+'/'+str(code)+'.htm'
        
elif parent>799 and parent<1000:
    if parent>799 and parent<805:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/800-804/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>804 and parent<810:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/805-809/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>809 and parent<820:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/810-819/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>819 and parent<830:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/820-829/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>829 and parent<840:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/830-839/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>839 and parent<849:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/840-848/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>849 and parent<855:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/850-854/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>859 and parent<870:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/860-869/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>869 and parent<880:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/870-879/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>879 and parent<888:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/880-887/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>889 and parent<898:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/890-897/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>899 and parent<905:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/900-904/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>904 and parent<910:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/905-909/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>909 and parent<920:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/910-919/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>919 and parent<925:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/920-924/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>924 and parent<930:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/925-929/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>929 and parent<940:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/930-939/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>939 and parent<950:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/940-949/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>949 and parent<958:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/950-957/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>957 and parent<960:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/958-959/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>959 and parent<980:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/960-979/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>979 and parent<990:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/980-989/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>989 and parent<996:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/990-995/'+str(parent_str)+'/'+str(code)+'.htm'
    elif parent>995 and parent<1000:
        query = 'http://www.icd9data.com/2015/Volume1/800-999/996-999/'+str(parent_str)+'/'+str(code)+'.htm'


# In[18]:


print(query)

