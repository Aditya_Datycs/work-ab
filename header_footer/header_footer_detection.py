import os
import re
import sys
import json
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from sklearn.cluster import KMeans
from nltk.tokenize import word_tokenize
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences


path = "Z:/RM-2018-ACA/202008/30120"
# path = sys.argv[1]

all_filename = []
all_pages = []

page_no = []
content = []

header_dict, footer_dict = dict(), dict()

# Header


# reading txt files and appending them in list filenames
filenames = []
for file in os.listdir(path+'/hocr'):
    if(file.endswith('.txt')):
        filenames.append(path+'/hocr/'+file)


# data cleaning for txt files and appending them page wise to pages[]
pages = []
n_pages = len(filenames)

for filename_i in range(n_pages):
    filename = filenames[filename_i]
    text = open(filename, 'r').read()
    text = text.lower()
    text = re.sub("\s\d+\s", " number ", text)
    text = re.sub("\d{1,2}/\d{1,2}/\d{2,4}|\d{6}", " date ", text)
    lines = []
    try:
        lines = re.findall("[^\n]+\n", text, flags=re.DOTALL)[:12]
    except:
        pass
    pages.append("".join(lines).replace("\n", " newline "))

# dropping pages which dont qualify
drop = []
for i in range(len(pages)):
    if "dob" not in pages[i]:
        if "mrn" not in pages[i]:
            drop.append(i)

drop.sort(reverse=True)

for i in drop:
    filenames.pop(i)
    pages.pop(i)

# NLP
#all_filename = filenames.copy()

# tokeniser with post padding
tokenizer = Tokenizer()
tokenizer.fit_on_texts(np.array(pages))
X = tokenizer.texts_to_sequences(np.array(pages))
X = pad_sequences(X, padding='post')

c_len = []
for i in range(X.shape[1]):
    x_no = X[:, i]
    c_len.append(len(set(x_no)))

i_min = c_len[1]
for i in range(1, len(c_len)-1):
    if(c_len[i] > i_min+2):
        i -= 2
        break

if(i > 1 and i < 20):
    X = X[:, :i]
else:
    if(i < 2):
        X = X[:, :2]
    else:
        X = X[:, :20]

clusters = []
for col in range(X.shape[1]):
    clusters.append(len(set(X[:, col])))

# clusters/=X.shape[1]


clusters = (int(sum(clusters)/X.shape[1]))


# Clustering

# k means on the pages
model = KMeans(n_clusters=clusters, init='k-means++').fit(X)
Y = model.predict(X)

# converting to np array
files = np.array(filenames)

# Checking clusters with one files

# finding clusters which can be combined
# (based on intersection between clusters with one element)
combined_clusters = []
for i in set(Y):
    if(len(X[Y == i]) == 1):
        max_len = (0, 0)
        for j in set(Y):
            if(i == j):
                continue
            intersection = np.intersect1d(X[Y == i][0], X[Y == j][0])
            if(len(intersection) < 3):
                continue
            else:
                if(max_len[1] < len(intersection)):
                    max_len = (j, len(intersection))
        if(max_len[1] != 0):
            combined_clusters.append((i, max_len[0]))


# combining the clusters
for i in combined_clusters:
    Y[np.where(Y == i[0])[0][0]] = i[1]

# Taking header_clusters

# making list header clusters (list of clusters)
header_clusters = []

files = np.array(filenames)

for i in set(Y):
    if(len(files[Y == i]) > 1):
        header_clusters.append(files[Y == i])

# Extracting clusters from hocr


def intersection(x, y):
    z = np.intersect1d(x, y)
    dob = False
    for dob_W in np.char.lower(z):
        if "dob" in dob_W or "mrn" in dob_W:
            dob = True
            break
    if(dob):
        return(z)
    else:
        return(np.array(x))


# getting global height
filename = filenames[0][:-3]+'hocr'
hocr = open(filename, 'r').read()
soup = BeautifulSoup(hocr, 'html.parser')
w = soup.find_all('div', class_='ocr_page')[0].get('title')
pageHeightGlobal = int(re.findall("bbox.(.*)\;", w)[0].split()[-1])
pageWidthGlobal = int(re.findall("bbox.(.*)\;", w)[0].split()[-2])

# work happens here

for cluster_files in header_clusters:
    filenames = []
    for cluster_filename in cluster_files:
        filenames.append(cluster_filename[:-3]+'hocr')
    pages = []
    n_pages = len(filenames)

    for filename_i in range(n_pages):
        filename = filenames[filename_i]
        hocr = open(filename, 'r').read()
        soup = BeautifulSoup(hocr, 'html.parser')

        words = soup.find_all('span', class_='ocrx_word')
        lines_soup = soup.find_all('span', class_='ocr_line')
        lines_soup.extend(soup.find_all('span', class_='ocr_header'))
        lines_soup.extend(soup.find_all('span', class_='ocr_textfloat'))
        lines_soup.extend(soup.find_all('span', class_='ocr_caption'))

        dataset = []
        for i in range(len(words)):
            nrow = []
            txt = words[i].get_text()
            txt = txt.replace(":", "")
            txt = txt.strip()
            if(len(txt) == 0):
                continue
            try:
                if(txt in list(np.array(dataset)[:, 0])):
                    continue
            except:
                pass

            w_id = words[i].get('id')

            for line_i in lines_soup:
                if(words[i] in line_i):
                    l_id = line_i.get('id')
                    break
            nrow.append(l_id)
            nrow.append(w_id)
            nrow.append(txt)
            title = words[i].get('title')
            bbox = re.findall("bbox.(.*)\;", title)[0].split()
            nrow.extend(bbox)
            nrow[6] = str(int(nrow[4])+int(nrow[6]))      # converting Y2=Y1+Y2
            nrow.append(pageWidthGlobal)
            dataset.append(nrow)
            if(int(nrow[6]) >= pageHeightGlobal/3):
                break

        pages.append(pd.DataFrame(np.array(dataset), columns=[
                     "Line_id", "Word_id", "Data", "X1", "Y1", "X2", "Y2", 'Width']))

    for i in range(len(pages)):
        pages[i]['Normalized_Data'] = pages[i]['Data']

    words = []
    for word_i in pages:
        nrow = []
        for j in range(len(word_i)):
            if(word_i['Normalized_Data'].values[j] == 'O' or word_i['Normalized_Data'].values[j].lower() == 'dob' or word_i['Normalized_Data'].values[j].lower() == 'mrn'):
                nrow.append(word_i['Data'].values[j].lower())
            else:
                nrow.append(word_i['Normalized_Data'].values[j].lower())
        word_i['Normalized_Data'] = nrow
        words.append(np.array(nrow).astype('str'))

    c = words[-1]
    if('dob' not in c):
        c = np.append(c, 'dob')
    if 'mrn' not in c:
        c = np.append(c, 'mrn')
    for i in range(len(words)-2, -1, -1):
        c = intersection(c, words[i])

    header_text = c

    stop = list(stopwords.words("english"))

    header_stops = []
    ht = list(header_text)
    for i in ht:
        if(i in stop):
            header_stops.append(i)

    for i in header_stops:
        ht.remove(i)

    header_text = np.array(ht)

    if('dob' not in header_text):
        ht = list(header_text)
        ht.append('dob')
        header_text = np.array(ht)

    if('mrn' not in header_text):
        ht = list(header_text)
        ht.append('mrn')
        header_text = np.array(ht)

    filenames = []

    for cluster_filename in cluster_files:
        filenames.append(cluster_filename[:-3]+'hocr')

    filenames = filenames[:n_pages]

    count = 0
    Y_vals = []
    index = []
    for page in range(len(pages)):
        img_path = filenames[page]

        # Finding Page height
        hocr = open(img_path, 'r').read()
        soup = BeautifulSoup(hocr, 'html.parser')
        w = soup.find_all('div', class_='ocr_page')[0].get('title')
        pageHeight = int(re.findall("bbox.(.*)\;", w)[0].split()[-1])

        Y_max = []
        for i in header_text:
            try:
                y = min(pages[page]['Y2'][pages[page]
                        ['Normalized_Data'] == i].astype('int').values)
                if(y < pageHeight/3):
                    Y_max.append(y)
            except:
                pass

        Y_max.sort()
        if(len(Y_max) == 0):
            continue
        try:
            Y_vals.append(Y_max[-1])
            index.append(page)
        except:
            pass

    count = 0
    skip = 0

    for page in range(len(Y_vals)):
        dob = False
        for dob_w in pages[index[page]]['Normalized_Data'].values:
            if('dob' in dob_w or 'mrn' in dob_w):
                dob = True
                break
        if(dob):
            img_path = filenames[index[page]]
            try:
                header_dict[img_path] = list(
                    pages[page][pages[index[page]]['Y2'] == str(Y_vals[page])].iloc[-1, [0, 1]])
            except:
                pass
        else:
            pass
    all_pages = all_pages + pages
    all_filename = all_filename + cluster_files.tolist()


def line_no(string1):
    return int(string1.replace('line_1_', ''))


for i in range(len(all_pages)):
    all_pages[i]['Path'] = all_filename[i]
    all_pages[i]['line'] = all_pages[i]['Line_id'].apply(line_no)

i = 0
for items in header_dict:
    line = line_no(header_dict[items][0])
    all_pages[i] = all_pages[i][all_pages[i]['line'] < line+1]
    i = i+1


df = pd.concat(all_pages)


all_keys = []
for items in df['Path'].unique():
    all_keys.append(items)

dict_keys = []
for items in header_dict:
    dict_keys.append(items.replace('.hocr', '.txt'))

not_headers = []
for items in all_keys:
    if items in dict_keys:
        continue
        # not_headers.append(items)
    not_headers.append(items)

df['Path'] = df['Path'].astype(str)
df = df[df['Path'].isin(dict_keys)]

g = df.values

new_df = []
for items in g:
    max_line = int(header_dict[items[9].replace(
        '.txt', '.hocr')][0].replace('line_1_', ''))
    if items[10] <= max_line:
        new_df.append(items)
    # break


new_df_arr = np.array(new_df)
new_df_arr = new_df_arr.T

df_new = pd.DataFrame(list(zip(new_df_arr[0].tolist(), new_df_arr[1].tolist(), new_df_arr[2].tolist(), new_df_arr[3].tolist(), new_df_arr[4].tolist(), new_df_arr[5].tolist(), new_df_arr[6].tolist(
), new_df_arr[7].tolist(), new_df_arr[8].tolist(), new_df_arr[9].tolist(), new_df_arr[10].tolist())), columns=['Line_id', 'Word_id', 'Data', 'X1', 'Y1', 'X2', 'Y2', 'Width', 'Normalized_Data', 'Path', 'line'])

n_filenames = []
for file in os.listdir(path+'/hocr'):
    if(file.endswith('.txt')):
        n_filenames.append(path+'/hocr/'+file)

n_filenames = []

for items in header_dict:
    page_no.append(items.replace('.hocr', ''))
    line = header_dict[items][0].replace('line_1_', '')
    f = open(items.replace('.hocr', '.txt'))
    s = f.readlines()
    list_n = []
    for st in s:
        if st != '\n':
            list_n.append(st)
    list_n = list_n[0:int(line)]
    list_n = " ".join(list_n).replace('\n', '')
    content.append(list_n)


output_dict = {"page_no": page_no, "content": content}
df = pd.DataFrame(list(zip(page_no, content)), columns=['file_no', 'content'])

# Finding Clusters from header_dict lines
filenames = list(header_dict.keys())
pages = []
for filename_i in range(len(filenames)):
    filename = filenames[filename_i]
    hocr = open(filename, 'r').read()
    soup = BeautifulSoup(hocr, 'html.parser')
    words = soup.find_all('span', class_='ocrx_word')
    lines_soup = soup.find_all('span', class_='ocr_line')
    lines_soup.extend(soup.find_all('span', class_='ocr_header'))
    lines_soup.extend(soup.find_all('span', class_='ocr_textfloat'))
    lines_soup.extend(soup.find_all('span', class_='ocr_caption'))

    dataset = []
    for i in range(len(words)):
        nrow = []
        txt = words[i].get_text()
        txt = txt.replace(":", "")
        txt = txt.strip()
        if(len(txt) == 0):
            continue
        try:
            if(txt in list(np.array(dataset)[:, 0])):
                continue
        except:
            pass

        w_id = words[i].get('id')

        for line_i in lines_soup:
            if(words[i] in line_i):
                l_id = line_i.get('id')
                break
        nrow.append(l_id)
        nrow.append(w_id)
        nrow.append(txt)
        title = words[i].get('title')
        bbox = re.findall("bbox.(.*).\;", title)[0].split()
        nrow.extend(bbox)

        nrow[6] = str(int(nrow[4])+int(nrow[6]))      # converting Y2=Y1+Y2

        dataset.append(nrow)
        if(int(nrow[6]) >= pageHeightGlobal/3):
            break

    pages.append(pd.DataFrame(np.array(dataset), columns=[
                 "Line_id", "Word_id", "Data", "X1", "Y1", "X2", "Y2"]))

X = []
for files_i in range(len(pages)):
    nrow = []
    try:
        nrow.extend(pages[files_i].iloc[0, 3:5].values)
        nrow.extend(pages[files_i][pages[files_i]['Line_id'] ==
                    header_dict[filenames[files_i]][0]].iloc[-1, 5:].values)
        X.append(nrow)
    except:
        pass


X = np.array(X)


# Footer

# Clustering
filenames = []
for file in os.listdir(path+'/hocr'):
    if(file.endswith('.txt')):
        filenames.append(file)

pages = []
n_pages = len(filenames)

for filename_i in range(n_pages):
    filename = path+'/hocr/'+filenames[filename_i]
    text = open(filename, 'r').read()
    text = text.lower()
    if("http" in text):
        text = re.sub("[-/]*http.*[ ]*", " url ", text)
    text = re.sub("(\n[ ]*)+\n", "\n", text, flags=re.DOTALL)
    text = re.sub("\s\d+\s", " number ", text)
    text = re.sub("\d{1,2}/\d{1,2}/\d{2,4}|\d{6}", " date ", text)
    lines = []
    try:
        lines = re.findall("[\n][^\n]+", text, flags=re.DOTALL)
        lines = lines[-10:]
    except:
        pass
    pages.append("".join(lines).replace("\n", " "))


# NLP
tokenizer = Tokenizer()
tokenizer.fit_on_texts(np.array(pages))
X = tokenizer.texts_to_sequences(np.array(pages))
X = pad_sequences(X, padding='pre')

c_len = []
for i in range(X.shape[1]-1, -1, -1):
    x_no = X[:, i]
    c_len.append(len(set(x_no)))

i_min = c_len[1]
for i in range(1, len(c_len)-1):
    if(c_len[i] > i_min+2):
        i = X.shape[1]-i+1
        break

if(i > X.shape[1]-20 and i < X.shape[1]-2):
    X = X[:, i:]
else:
    if(i > X.shape[1]-3):
        X = X[:, X.shape[1]-2:]
    else:
        X = X[:, X.shape[1]-20:]

Y = X.tolist()
clusters = len(list(map(list, set(map(lambda i: tuple(i), Y)))))


# Clustering
model = KMeans(n_clusters=clusters, init='k-means++').fit(X)
Y = model.predict(X)


files = np.array(filenames)

# Checking clusters with one files

combined_clusters = []
for i in set(Y):
    if(len(X[Y == i]) == 1):
        max_len = (0, 0)
        for j in set(Y):
            if(i == j):
                continue
            intersection = np.intersect1d(X[Y == i][0], X[Y == j][0])
            if(len(intersection) < 3):
                continue
            else:
                if(max_len[1] < len(intersection)):
                    max_len = (j, len(intersection))
        if(max_len[1] != 0):
            combined_clusters.append((i, max_len[0]))


for i in combined_clusters:
    Y[np.where(Y == i[0])[0][0]] = i[1]


# Taking footer_clusters


footer_clusters = []

files = np.array(filenames)

for i in set(Y):
    if(len(files[Y == i]) > 1):
        footer_clusters.append(files[Y == i])


# Extracting clusters from hocr


def intersection(x, y):
    z = np.intersect1d(x, y)
    return z


filename = path+'/hocr/'+filenames[0][:-3]+'hocr'
hocr = open(filename, 'r').read()
soup = BeautifulSoup(hocr, 'html.parser')
w = soup.find_all('div', class_='ocr_page')[0].get('title')
pageHeightGlobal = int(re.findall("bbox.(.*)\;", w)[0].split()[-1])


cluster_count = 0

for cluster_files in footer_clusters:
    cluster_count = cluster_count+1
    filenames = []
    for cluster_filename in cluster_files:
        filenames.append(cluster_filename[:-3]+'hocr')
    pages = []

    n_pages = len(filenames)

    for filename_i in range(n_pages):
        filename = path+'/hocr/'+filenames[filename_i]
        hocr = open(filename, 'r').read()
        soup = BeautifulSoup(hocr, 'html.parser')

        words = soup.find_all('span', class_='ocrx_word')
        lines_soup = soup.find_all('span', class_='ocr_line')
        lines_soup.extend(soup.find_all('span', class_='ocr_header'))
        lines_soup.extend(soup.find_all('span', class_='ocr_textfloat'))
        lines_soup.extend(soup.find_all('span', class_='ocr_caption'))
        dataset = []
        for i in range(len(words)):
            nrow = []
            txt = words[i].get_text()
            txt = txt.replace(":", "")
            txt = txt.strip()
            if("http" in txt):
                txt = re.sub("[-/]*http.*[ ]*", "url", txt)
            txt = re.sub("\d{1,2}/\d{1,2}/\d{2,4}|\d{6}", "date", txt)
            if(len(txt) == 0):
                continue
            try:
                if(txt in list(np.array(dataset)[:, 0])):
                    pass
            except:
                pass

            w_id = words[i].get('id')
            for line_i in lines_soup:
                if(words[i] in line_i):
                    l_id = line_i.get('id')
                    break
            nrow.append(l_id)
            nrow.append(w_id)
            nrow.append(txt)
            title = words[i].get('title')
            bbox = re.findall("bbox.(.*).\;", title)[0].split()
            nrow.extend(bbox)

            if(int(nrow[4]) <= pageHeightGlobal*4/5):
                continue
            dataset.append(nrow)

        if(len(dataset) > 0):
            pages.append(pd.DataFrame(np.array(dataset), columns=[
                         "Line_id", "Word_id", "Data", "X1", "Y1", "X2", "Y2"]))

    for i in range(len(pages)):
        pages[i]['Normalized_Data'] = pages[i]['Data']

    words = []
    for word_i in pages:
        nrow = []
        for j in range(len(word_i)):
            if(word_i['Normalized_Data'].values[j] == 'O' or word_i['Normalized_Data'].values[j].lower() == 'dob' or word_i['Normalized_Data'].values[j].lower() == 'mrn'):
                nrow.append(word_i['Data'].values[j].lower())
            else:
                nrow.append(word_i['Normalized_Data'].values[j].lower())
        word_i['Normalized_Data'] = nrow
        words.append(np.array(nrow).astype('str'))

    try:
        c = words[-1]
    except:
        continue

    for i in range(len(words)-2, -1, -1):
        c = intersection(c, words[i])
    footer_text = c

    stop = list(stopwords.words("english"))

    footer_stops = []
    ht = list(footer_text)
    for i in ht:
        if(i in stop):
            footer_stops.append(i)

    for i in footer_stops:
        ht.remove(i)

    footer_text = np.array(ht)

    footer_words = ['printed', "generated", "date", "url", 'dob', 'dob:']

    for f_w in footer_words:
        if(f_w not in footer_text):
            ft = list(footer_text)
            ft.append(f_w)
            footer_text = np.array(ft)

    filenames = []

    for cluster_filename in cluster_files:
        filenames.append(cluster_filename[:-3]+'hocr')

    filenames = filenames[:n_pages]

    page = 1

    count = 0
    Y_vals = []
    index = []
    for page in range(len(pages)):
        img_path = path+'/hocr/'+filenames[page]
        hocr = open(img_path, 'r').read()
        soup = BeautifulSoup(hocr, 'html.parser')
        w = soup.find_all('div', class_='ocr_page')[0].get('title')
        pageHeight = int(re.findall("bbox.(.*)\;", w)[0].split()[-1])

        Y_min = []
        for i in footer_text:
            found = 0
            try:
                y = max(pages[page]['Y1'][pages[page]
                        ['Normalized_Data'] == i].astype('int').values)
                if(y >= pageHeight*4/5):
                    Y_min.append(y)
                    found = 1
            except:
                pass

        Y_min.sort()

        if(len(Y_min) == 0):
            continue
        try:
            Y_vals.append(Y_min[0])
            index.append(page)
        except:
            pass

    count = 0
    skip = 0
    for page in range(len(Y_vals)):
        if(True):
            if page == index[page]:
                img_path = filenames[index[page]]
                footer_dict[path+'/hocr/'+img_path] = list(
                    pages[page][pages[index[page]]['Y1'] == str(Y_vals[page])].iloc[-1, [0, 1]])

        else:
            pass

print("Footers ---> ", footer_dict)

# Combining


def combineDicts(dict_H, dict_F):
    combined_pages = list(set(dict_H).union(set(dict_F)))
    combined_pages.sort()

    combined_dict = dict()
    for page in combined_pages:
        HF = []
        if(page in dict_H):
            HF.append(dict_H[page])
        else:
            HF.append([])

        if(page in dict_F):
            HF.append(dict_F[page])
        else:
            HF.append([])
        combined_dict[page] = HF
    return(combined_dict)


header_footer_dict = combineDicts(header_dict, footer_dict)

# for items in os.listdir(path + "/hocr"):
#     if items.endswith(".hocr"):
#         if path + "/hocr/" + items not in header_footer_dict.keys():
#             header_footer_dict[path + "/hocr/" + items] = [[], []]

print(header_footer_dict)
